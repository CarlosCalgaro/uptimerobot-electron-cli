const axios = require('axios');

window.$ = window.jQuery = require("jquery");

const uptime = axios.create({
    baseURL: 'https://api.uptimerobot.com/v2/',
    timeout: 7000,
  });


$("#single-site").on( "submit", function(event){
    event.preventDefault();
    addNewMonitor();
})

function getMonitors(){
    uptime.post('getMonitors', {
        api_key: 'u608132-d1ac017e28ad9b629e873270', format: 'json', logs: '1' 
    }).then(function (response) {
        window.monitors = response.data.monitors;
        $("#resposta").html("");
        $("#resposta").html("<thead><tr><th>Nome do Site</th><th>Status</th></tr></thead>") 
        for(i=0; i<response.data.monitors.length; i++){
            append_string =  "<tbody><td>"+ response.data.monitors[i].friendly_name + 
                             "</td><td>"+ getIcon(response.data.monitors, i) +"</td></tbody>" 
            $("#resposta").append(append_string);
        }
        console.log(response.data);
    })
    
}

function addNewMonitor(){
    uptime.post("newMonitor", {
        api_key: 'u608132-d1ac017e28ad9b629e873270', format: 'json',
        friendly_name: $("#friendly_name").val(),
        url: $("#name").val(),
        type: 1
    }).then(function (response) {
      console.log("Adicionado com sucesso");
      getMonitors();
    })
}

function getIcon(array, index){
    if(array[index].status === 2){
        return "<i class=\"green-text material-icons\">check</i>"
    }else{
        return "<i class=\"red-text material-icons\">cancel</i>"
    }
}


/**
 * Page initialization
 */

getMonitors();

setInterval(function(){ 
    getMonitors();
}, 5000);
