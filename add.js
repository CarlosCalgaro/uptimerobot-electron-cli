/**
 * uptime.post("newMonitor", {
        api_key: 'u608132-d1ac017e28ad9b629e873270', format: 'json',
        friendly_name: $("#friendly_name").val(),
        url: $("#name").val(),
        type: 1
    }).then(function (response) {
        M.toast({html: 'Adicionado Com Sucesso'})
    }) 
 */
const axios = require('axios');

window.$ = window.jQuery = require("jquery");

const uptime = axios.create({
    baseURL: 'https://api.uptimerobot.com/v2/',
    timeout: 7000,
  });


function addNewMonitor(){
    uptime.post("newMonitor", {
        api_key: 'u608132-d1ac017e28ad9b629e873270', format: 'json',
        friendly_name: $("#friendly_name").val(),
        url: $("#name").val(),
        type: 1
    }).then(function (response) {
        alert("Adicionado com Sucesso!")
    }) 
}


module.exports = addNewMonitor;