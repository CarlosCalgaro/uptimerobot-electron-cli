const electron = require('electron');
const axios = require('axios');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

let mainWindow;
app.on('ready', function(){
    mainWindow = new BrowserWindow({width: 1080, height: 643});
    mainWindow.loadURL('file://' + __dirname + '/index.html');
  //  mainWindow.openDevTools();
    mainWindow.on('closed', function(){
        mainWindow = null;
    })

})